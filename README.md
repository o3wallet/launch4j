# Cross-platform Java executable wrapper
Launch4j is a cross-platform tool for wrapping Java applications distributed as jars in lightweight Windows native executables. The executable can be configured to search for a certain JRE version or use a bundled one, and it's possible to set runtime options, like the initial/max heap size. The wrapper also provides better user experience through an application icon, a native pre-JRE splash screen, and a Java download page in case the appropriate JRE cannot be found.

##Features
* Launch4j wraps jars in Windows native executables and allows to run them like a regular Windows program.
* It's possible to wrap applications on Windows, Linux and Mac OS X.
* Also creates launchers for jars and class files without wrapping
* Supports executable jars and dynamic classpath resolution using environment variables and wildcards.
* Doesn't extract the jar from the executable.
* Native pre-JRE splash screen in BMP format shown until the Java application starts.
* Initial priority and single application instance features.
* Works with a bundled JRE or searches for newest Sun or IBM JRE / JDK in given version range and type (64-bit or 32-bit).
* Supports GUI and console apps.
* Supports Windows application manifests.
* Allows to set the initial/max heap size also dynamically in percent of free memory.
* JVM options: set system properties, tweak the garbage collection.
* Runtime JVM options from an .l4j.ini file.
* Runtime command line switches to change the compiled options.
* Access to environment variables, the registry and executable file path through system properties.
* Set environment variables.
* Ability to restart the application based on exit code.
* Custom version information shown by Windows Explorer.
* Digital signing of the executable with sign4j.
* Supports Windows Security Features of the Windows 8 certification kit.
* GUI and command line interface.
* Build integration through an Ant task and a Maven Plugin.
* Lightweight: 35 KB!
* The wrapped program works on Windows only, but Launch4j works on Windows, Linux and Mac OS X.
#Forked from http://launch4j.sourceforge.net/